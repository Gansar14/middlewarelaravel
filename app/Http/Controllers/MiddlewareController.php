<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
class MiddlewareController extends Controller
{

    // public function __construct(){
    //     $this->middleware(['auth', 'superadmin'])->except(['admin','user']);
    // }
    public function superadmin()
    {
        return 'Superadmin berhasil masuk';
    }
    public function admin()
    {
        // $this->middleware(['auth', 'superadmin'])->except(['admin','user']);
        return 'admin berhasil masuk';
    }
    public function user()
    {
        return 'user berhasil masuk';
    }
}
